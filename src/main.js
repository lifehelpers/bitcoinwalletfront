const BASE_URL = 'http://localhost:8080/';

new Vue({
    el: '#app',
    data() {
        return {
            info: null,
            userId: 3,
            username: 'Stranger',
            cryptos: null,
            crypto_form_data: {
                userCryptoCurrencyId: 0,
                cryptoCurrencyId: 2,
                walletLocation: ""
            },
            user_cryptos: []
        }
    },
    created: function () {
        this.crypto_form_data.dateOfPurchase = new Date().toISOString().split('T')[0];
        this.loadData(this.userId);
    },
    methods: {
        promptDelete: function (crypto) {
            const cryptoName = this.getCryptoNameFromId(crypto.cryptoCurrencyId);
            if (confirm('U sure u wanna delete ' + crypto.amount + ' of your '
                + cryptoName + ' money?')) {
                this.deleteFromUserWallet(crypto);
                alert(crypto.amount + ' ' + cryptoName + 's deleted :(');
                window.location.reload();
            }
        },
        promptAdd: function (crypto_form_data) {
            //TODO add user crypto (data)
            this.addToUserWallet(crypto_form_data);
            alert(' Consider it done :)');
            window.location.reload();
        },
        loadData: function (userId) {
            this.fetchCryptoCurrencies();
            axios.all([
                axios.get(BASE_URL + 'users/' + userId),
                axios.get(BASE_URL + '/wallet/users/' + userId)
            ])
                .then(axios.spread((userData, userCryptoData) => {
                    this.setUserData(userData.data);
                    this.setUserWalletData(userCryptoData.data);
                }));
        },
        setUserData: function (userData) {
            this.username = userData.username;
            this.user_id = userData.userId;
        },
        setUserWalletData: function (userCryptoData) {
            this.user_cryptos = userCryptoData;
        },
        deleteFromUserWallet: function (crypto) {
            const url = BASE_URL + 'wallet/' + crypto.userCryptoCurrencyId;
            axios.delete(url);
        },
        addToUserWallet: function (crypto_form_data) {
            const url = BASE_URL + 'wallet';
            crypto_form_data.userId = this.user_id;
            axios.post(url, crypto_form_data);
        },
        getCryptoNameFromId: function (id) {
            const cryptoWithGivenId = this.cryptos
                .find(crypto => crypto.cryptoCurrencyId === id);
            return cryptoWithGivenId.name;
        },
        fetchCryptoCurrencies: function () {
            axios.get(BASE_URL + 'crypto_currencies')
                .then(cryptoData => {
                    this.cryptos = cryptoData.data;
                })
        },
        formatDate: function (date) {
            return new Date(date).toLocaleDateString();
        }
    }
})
;
